本程序为开源程序，仅限测试和研究使用，如果有人使用本人程序做任何非法事情，本人不负任何责任法律责任
### 特性 ：<br>
1.站群<br>
2.开源的，能白嫖<br>
3.自己研究<br>
4.还会更新的<br>
5.支持 小说 视频 图片<br>
6. 懂得人都明白 <br>
### 安装方法：<br>
1. 配置伪静态规则<br>
2.修改 /Core/Mysql/Config.php<br>
3.将数据库文件导入数据库。（压缩包自带的sql文件，无数据。      可直接建站版，去蓝奏下载，链接我会扔在最下面）<br>
4.在数据库中找到"colin_site"这个数据表。<br>
5.将"url"字段中的127.0.0.1改成自己的域名。<br>
6.安装完成。<br>
### 后台默认信息：
后台地址/Admin/login.php<br>
账号admin<br>
密码123456<br>
### 伪静态规则：

Apache

```
<IFMODULE mod_rewrite.c> 
RewriteEngine ON
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule (.+) Page/$1
</IFMODULE>
```

Nginx

```
if (!-d $request_filename){
	set $rule_0 1$rule_0;
}
if (!-f $request_filename){
	set $rule_0 2$rule_0;
}
if ($rule_0 = "21"){
	rewrite /(.+) /Page/$1;
}
```
### 采集方法：

宝塔-计划任务-访问URL<br>
你的域名/Cron/Video/video_1.php 等<br>
设置时间即可自动采集<br>
 **注意** ：如果更改分类，需修改采集文件<br>
### API调用方法：
/Api/api_video.php/my=sj 随机视频<br>
/Api/api_video.php/my=gc 国产<br>
/Api/api_video.php/my=yz 亚洲<br>
/Api/api_video.php/my=om 欧美<br>
/Api/api_video.php/my=dm 动漫<br>
以上，只是列举视频<br>
-------------------------------
本程序需改动地方，请自行研究。<br>
---------------------------------
By WuJiu_SS<br>
QQ 1713281933<br>
如果你喜欢本程序，可以进行赞助<br>
QQ红包赞助：加QQ就好了<br>
微信赞助：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0508/182252_361db239_1778060.png "微信.png")
支付宝赞助：![输入图片说明](https://images.gitee.com/uploads/images/2020/0508/182316_18b458df_1778060.jpeg "支付宝.jpg")
不好意思，我骗了你，没蓝奏，只有百度<br>
链接: https://pan.baidu.com/s/1qWwVUP6ezlzI5RfGjWX8Xg 提取码: yr5c